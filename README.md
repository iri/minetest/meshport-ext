# Meshport-ext [WIP]

This is a clone of the Minetest mod [Meshport](https://github.com/random-geek/meshport).

We are trying to improve some features following the feedback from the [UNEJ project](https://minetest.net/education/) in France.

Educators, designers, urban planners, and architects worked with students to rebuild real areas in Minetest. This was made possible thanks to the [IGN (French National Institute of Geographic and Forest Information) service](https://minecraft.ign.fr/), which allows creating Minetest maps from geographical data of France or the world (OSM data).

## Roadmap

- Addition of a module for displaying and exporting 3D models to our Minetest server hosting open-source templates.
- Improving the 3D files exporting options.
- Adding new file formats.
- Injection of a gallery instance.
- Deployment of a demonstration instance.

---

## License

Licensed under the AGPL (Affero General Public License).

## Funding

[2]: https://creativecommons.org/licenses/by/4.0/
